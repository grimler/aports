# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>

pkgname=font-iosevka
pkgver=11.2.1
pkgrel=0
pkgdesc="Versatile typeface for code, from code."
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab
	$pkgname-aile
	$pkgname-etoile
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-aile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-etoile-$pkgver.zip
	"

builddir="$srcdir"

_install_font_ttc() {
	font="$srcdir/$1.ttc"
	install -Dm644 "$font" -t "$subpkgdir"/usr/share/fonts/TTC
}

package() {
	for pkg in $subpackages; do
		depends="$depends $pkg"
	done
	mkdir -p "$pkgdir"
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	_install_font_ttc "iosevka"
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	_install_font_ttc "iosevka-slab"
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	_install_font_ttc "iosevka-curly"
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	_install_font_ttc "iosevka-curly-slab"
}

aile() {
	pkgdesc="$pkgdesc (Iosevka Aile)"
	_install_font_ttc "iosevka-aile"
}

etoile() {
	pkgdesc="$pkgdesc (Iosevka Etoile)"
	_install_font_ttc "iosevka-etoile"
}

sha512sums="
3f88d206faf4448caac9074b55e37805d4f284414118fd90353cae00875f784f02fcbf13a9547c87d3a77c02d07da01ac34800f69e382d5bf28da564b9c932d9  super-ttc-iosevka-11.2.1.zip
3f10626a3b4065cfd924f33b995bac703d5d285c74e279b6cf219a1d0556d5d3f4a89faeb7d47d396dbd6fa7234f99478c9d2ce7567c7d26e40c6e0e9749b8cb  super-ttc-iosevka-slab-11.2.1.zip
93406b03d933c4edc65a3e396d020c12f289b57c4e84f4b6c0947b8b677dcc597dcd3f6b32a9417de0de207a36978b4393d736105a452c960808938d322de262  super-ttc-iosevka-curly-11.2.1.zip
96f8c3b8c943da41fe9667f9bbe7945a594f9903b431c81b0b566af339269f9f3833868292f4652d73ce5dc261ffd8d822b1230f7c88071b8dc0d06529c17db2  super-ttc-iosevka-curly-slab-11.2.1.zip
c021ab6d4ee5f9bffa5b2d514d81537cc74e48b874ff0033b114fa2bc8b79ffab9e885a61969947d93c4e7e19375b4bde9e8a6429f3a9a43a6fa7540c8248e4a  super-ttc-iosevka-aile-11.2.1.zip
ef3b56041d53481fa3947c99fd1cb37de1cacc4fba002525932972417c1505e3e7a04fb881d7e4162af02aafbf2fc6060d56f89ced5558381cf4809068df4806  super-ttc-iosevka-etoile-11.2.1.zip
"
