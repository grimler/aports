# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=nvui
pkgver=0.2.1
pkgrel=0
pkgdesc="A modern frontend for Neovim"
url="https://github.com/rohit-px2/nvui"
arch="all"
license="MIT"
depends="hicolor-icon-theme neovim"
makedepends="
	boost-dev
	catch2
	cmake
	fmt-dev
	msgpack-c-dev
	ninja
	qt5-qtbase-dev
	qt5-qtsvg-dev
	"
source="https://github.com/rohit-px2/nvui/archive/v$pkgver/nvui-$pkgver.tar.gz
	fhs-paths.patch
	nvui.sh
	$pkgname.desktop
	"

prepare() {
	default_prepare

	# Remove images that are used only in Readme.
	rm -rf assets/display
}

build() {
	local crossopts=
	if [ "$CBUILD" != "$CHOST" ]; then
		crossopts="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -G Ninja -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=None \
		$crossopts .
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE LANG=C ctest
}

package() {
	install -D -m755 build/nvui -t "$pkgdir"/usr/libexec/
	install -D -m755 "$srcdir"/nvui.sh "$pkgdir"/usr/bin/nvui

	install -d "$pkgdir"/usr/share/$pkgname
	cp -r assets vim "$pkgdir"/usr/share/$pkgname/

	install -D -m644 "$srcdir"/$pkgname.desktop -t "$pkgdir"/usr/share/applications/
	install -D -m644 assets/appicon.png "$pkgdir"/usr/share/icons/hicolor/128x128/apps/$pkgname.png
}

sha512sums="
e5e1eeb7f63455bb9d5b128c10056940c903a428329ad6634f6739d707faa7b6316d2fc8e7572b2ef4ee8281c36e2bac4828e2916c13298ba1abca9bc6a59649  nvui-0.2.1.tar.gz
3f657ee1d23b0d3e9b2fe024f11527ced00359505391266510823c3b0e077e547decddc5d40b1e6eba796603062b5771a22669995ffb73091370796263006bb7  fhs-paths.patch
6b4f6cc14e72a38ee74da0b2f56314362a9454d283fe4a509ca5357e056d1149e06e2560a6809f9c264e117c2a4f0ab9fa4768c515da0956ed4875776b48c8f5  nvui.sh
03c2ea47f3b7fb748978fbe4dc54d475db2258e6078061595f0da8fa02a4eba0610138f1b2c14d8aa6a25dc7299abb687f53a44d0a1eedd3da06587107679ecf  nvui.desktop
"
